FactoryGirl.define do
  factory :auction do
    title Faker::Lorem.sentence(5)
    date Date.current
    slug Faker::Lorem.sentence(3).parameterize
    address '635 Papineau, Saint-Hyacinthe, Quebec'
    address_for_geocoding '635 Papineau, Saint-Hyacinthe, Quebec'
    published true
  end

  factory :image do
    association :parent, :factory => :auction
  end

  factory :ad_post do
    title Faker::Lorem.sentence(1)
    body Faker::Lorem.sentence(1)
    slug 'foo'
    short_description Faker::Lorem.sentence(2)
    published true
  end
end
