require 'rails_helper'

describe Auction do
  describe 'geocoding' do
    context 'when an auction is validated with an address_for_geocoding' do
      subject { FactoryGirl.build(:auction) }

      it "should be geocoded" do
        subject.valid?
        expect(subject).to be_geocoded
      end
    end
  end

  describe '#slug=' do
    subject { Auction.new }

    it 'makes the slug uri friendly' do
      subject.slug = 'foo bar'
      expect(subject.slug).to eq('foo-bar')
    end
  end

  describe '#google_map' do
    context 'when the auction has coordinates' do
      subject { FactoryGirl.create(:auction).google_map }

      it 'returns the google map url' do
        expect(subject).to eq("http://maps.google.com/maps?q=40.7143528+-74.0059731")
      end
    end

    context 'when the auction doesn’t have coordinates' do
      subject { FactoryGirl.build(:auction, address_for_geocoding: '').google_map }

      it 'returns nil' do
        expect(subject).to be_nil
      end
    end
  end

  describe '#geocoded?' do
    context 'when the auction has a longitude and latitude' do
      subject { FactoryGirl.create(:auction).geocoded? }

      it 'returns true' do
        expect(subject).to be_truthy
      end
    end

    context 'when the auction does not have a longitude and latitude' do
      subject { FactoryGirl.create(:auction, address_for_geocoding: '').geocoded? }

      it 'returns false' do
        expect(subject).to be_falsy
      end
    end
  end

  describe '#slug_with_id' do
    subject { Auction.new(id: 1, slug: 'foo') }

    it 'returns the slug prefixed with the id of the post' do
      expect(subject.slug_with_id).to eq('1-foo')
    end
  end

  describe '#outdated' do
    context 'when the auction date is passed' do
      subject { FactoryGirl.build(:auction, date: Date.yesterday) }

      it 'returns true' do
        expect(subject.outdated?).to be_truthy
      end
    end

    context 'when the auction date is today' do
      subject { FactoryGirl.build(:auction, date: Date.current) }

      it 'returns false' do
        expect(subject.outdated?).to be_falsy
      end
    end

    context 'when the auction date is tomorrow or later' do
      subject { FactoryGirl.build(:auction, date: Date.tomorrow) }

      it 'returns false' do
        expect(subject.outdated?).to be_falsy
      end
    end
  end

  describe '.upcoming' do
    before do
      @auctions = []
      @auctions << FactoryGirl.create(:auction)
      @auctions << FactoryGirl.create(:auction, date: Date.yesterday)
    end

    subject { Auction.upcoming }

    it 'returns only the upcoming auctions' do
      expect(subject.size).to eq(1)
      expect(subject.first.id).to eq(@auctions.first.id)
    end
  end

  describe '.published' do
    before do
      @published_auction = FactoryGirl.create(:auction)
      @unpublished_auction = FactoryGirl.create(:auction, published: false)
    end

    subject { Auction.published }

    it 'returns the published auctions' do
      expect(subject.first.id).to eq(@published_auction.id)
    end

    it 'does not return the unpublished auctions' do
      expect(subject).not_to include(@unpublished_auction.id)
    end
  end

  describe '.ordered_by_date' do
    before do
      @auctions = []
      @auctions << FactoryGirl.create(:auction, date: Date.tomorrow)
      @auctions << FactoryGirl.create(:auction)
    end

    subject { Auction.ordered_by_date }

    it 'returns the auctions in chronological order' do
      expect(subject.first.date).to be < subject[1].date
    end
  end

  describe '.find_by_slug_with_id' do
    context 'when an auction is found' do
      before do
        @auction = FactoryGirl.create(:auction)
      end

      subject { Auction.find_by_slug_with_id(@auction.slug_with_id) }

      it 'returns the auction' do
        expect(subject.id).to eq(@auction.id)
      end
    end

    context 'when an auction is not found' do
      it 'returns nil' do
        p = Auction.find_by_slug_with_id('1-meh')
        expect(p).to be_nil
      end
    end
  end
end
