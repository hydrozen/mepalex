require 'rails_helper'

describe Image do
  describe 'attributes' do
    subject { FactoryGirl.build(:image) }

    it "should have a parent" do
      expect(subject.parent).not_to be_nil
    end
  end

  describe 'before_filter' do
    describe 'set_position' do
      context 'when it’s the first image created for this parent' do
        it 'sets the position to 1' do
          image = FactoryGirl.create(:image, skip_processing: false)
          expect(image.position).to eq(1)
        end
      end

      context 'when it’s the 2nd image created for this parent' do
        it 'sets the position to the last position incremented by 1' do
          image = FactoryGirl.create(:image, skip_processing: false)
          second_image = FactoryGirl.create(:image, parent: image.parent, skip_processing: false)
          expect(second_image.position).to eq(image.position + 1)
        end
      end
    end
  end
end
