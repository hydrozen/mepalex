require 'rails_helper'

describe StatusUpdate do
  describe 'attributes' do
    subject { StatusUpdate.new(:status => "foo", :link => "http://www.google.com") }

    it "has a status" do
      expect(subject.status).to eq('foo')
    end

    it "has a link" do
      expect(subject.link).to eq('http://www.google.com')
    end
  end

  # describe "#post_to_twitter" do
  #   subject { StatusUpdate.new(:status => 'test one two', :link => "http://www.google.com")}

  #   it 'posts the status update to twitter', :vcr do
  #     res = subject.post_to_twitter
  #     expect(res.class).to be(Twitter::Tweet)
  #   end
  # end
end
