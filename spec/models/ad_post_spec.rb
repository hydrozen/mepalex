require 'rails_helper'

describe AdPost do
  describe '#slug=' do
    subject { AdPost.new }

    it 'makes the slug uri friendly' do
      subject.slug = 'foo bar'
      expect(subject.slug).to eq('foo-bar')
    end
  end

  describe '#slug_with_id' do
    subject { AdPost.new(id: 1, slug: 'foo') }

    it 'returns the slug prefixed with the id of the post' do
      expect(subject.slug_with_id).to eq('1-foo')
    end
  end

  describe '.find_by_slug_with_id' do
    context 'when a post is found' do
      before do
        @post = FactoryGirl.create(:ad_post)
      end

      subject { AdPost.find_by_slug_with_id(@post.slug_with_id) }

      it 'returns the post' do
        expect(subject.id).to eq(@post.id)
      end
    end

    context 'when a post is not found' do
      it 'returns nil' do
        p = AdPost.find_by_slug_with_id('1-meh')
        expect(p).to be_nil
      end
    end
  end

  describe '.published' do
    before do
      @published_post = FactoryGirl.create(:ad_post)
      @unpublished_post = FactoryGirl.create(:ad_post, published: false)
    end

    subject { AdPost.published }

    it 'returns the published posts' do
      expect(subject.first.id).to eq(@published_post.id)
    end

    it 'does not return the unpublished posts' do
      expect(subject).not_to include(@unpublished_post.id)
    end
  end
end
