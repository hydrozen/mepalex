class AddGoogleMapsUrlToAuction < ActiveRecord::Migration
  def change
    add_column :auctions, :google_maps_url, :string
  end
end
