class ChangeNewsletterBodyToText < ActiveRecord::Migration
  def up
    change_column :newsletters, :body, :text
  end

  def down
    change_column :newsletters, :body, :string
  end
end
