class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.integer     :parent_id,   :null => false
      t.string      :parent_type, :null => false
      t.string      :file
      t.timestamps
    end
    add_index :images, [:parent_id, :parent_type]
  end
end
