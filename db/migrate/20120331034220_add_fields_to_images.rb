class AddFieldsToImages < ActiveRecord::Migration
  def change
    add_column :images, :position, :integer, :null => false, :default => 0
    add_column :images, :caption, :string, :null => false, :default => ""
  end
end
