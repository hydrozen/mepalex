class CreateAdPosts < ActiveRecord::Migration
  def change
    create_table :ad_posts do |t|
      t.string :title, :default => "", :null => false
      t.text :body, :default => "", :null => false
      t.timestamps
    end
  end
end
