class AddShortDescriptionToAdPosts < ActiveRecord::Migration
  def change
    add_column :ad_posts, :short_description, :text
  end
end
