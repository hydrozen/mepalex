class AddShortDescriptionToAuction < ActiveRecord::Migration
  def change
    add_column :auctions, :short_description, :text
  end
end
