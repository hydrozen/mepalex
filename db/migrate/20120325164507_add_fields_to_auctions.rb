class AddFieldsToAuctions < ActiveRecord::Migration
  def change
    add_column :auctions, :published, :boolean,
      :null => false, :default => false
    add_column :auctions, :address_for_geocoding, :string,
      :null => false, :default => ""
  end
end
