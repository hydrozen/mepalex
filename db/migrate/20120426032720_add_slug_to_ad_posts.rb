class AddSlugToAdPosts < ActiveRecord::Migration
  def change
    add_column :ad_posts, :slug, :string, :null => false, :default => ""
  end
end
