class CreateAuctionsNewsletters < ActiveRecord::Migration
  def up
    create_table :auctions_newsletters, :id => false do |t|
      t.references :newsletter
      t.references :auction
    end
    add_index :auctions_newsletters, :newsletter_id
  end

  def down
    drop_table :auctions_newsletters
  end
end
