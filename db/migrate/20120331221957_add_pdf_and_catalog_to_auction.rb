class AddPdfAndCatalogToAuction < ActiveRecord::Migration
  def change
    add_column :auctions, :pdf, :string
    add_column :auctions, :catalog, :string
  end
end
