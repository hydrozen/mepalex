class AddSlugToAuction < ActiveRecord::Migration
  def change
    add_column :auctions, :slug, :string, :null => false, :default => ""
  end
end
