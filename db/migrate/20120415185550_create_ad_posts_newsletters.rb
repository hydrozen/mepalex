class CreateAdPostsNewsletters < ActiveRecord::Migration
  def change
    create_table :ad_posts_newsletters, :id => false do |t|
      t.references :newsletter
      t.references :ad_post
    end
    add_index :ad_posts_newsletters, :newsletter_id
  end
end
