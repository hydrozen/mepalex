class AddIndexToImages < ActiveRecord::Migration
  def change
    add_index :images, [:parent_id, :parent_type, :position]
  end
end
