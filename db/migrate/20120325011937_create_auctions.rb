class CreateAuctions < ActiveRecord::Migration
  def change
    create_table :auctions do |t|
      t.string      :title,   :null => false, :default => ""
      t.text        :body,    :null => false, :default => ""
      t.date        :date,    :null => false
      t.string      :address, :null => false, :default => ""
      t.float       :longitude
      t.float       :latitude
      t.timestamps
    end
  end
end
