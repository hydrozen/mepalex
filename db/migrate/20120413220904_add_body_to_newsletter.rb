class AddBodyToNewsletter < ActiveRecord::Migration
  def change
    add_column :newsletters, :body, :string, :default => ""
  end
end
