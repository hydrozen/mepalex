class ChangeGoogleMapsUrlType < ActiveRecord::Migration
  def change
    change_column :auctions, :google_maps_url, :text
  end
end
