class CreateNewsletters < ActiveRecord::Migration
  def change
    create_table :newsletters do |t|
      t.datetime :sent_at
      t.timestamps
    end
  end
end
