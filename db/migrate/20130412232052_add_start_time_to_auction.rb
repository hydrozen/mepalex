class AddStartTimeToAuction < ActiveRecord::Migration
  def change
    add_column :auctions, :start_time, :string
  end
end
