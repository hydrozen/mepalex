class AddImageToAuction < ActiveRecord::Migration
  def change
    add_column :auctions, :image, :text
  end
end
