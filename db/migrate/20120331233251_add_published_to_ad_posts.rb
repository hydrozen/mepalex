class AddPublishedToAdPosts < ActiveRecord::Migration
  def change
    add_column :ad_posts, :published, :boolean, :default => false, :null => false
  end
end
