# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140108194528) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "ad_posts", force: true do |t|
    t.string   "title",             default: "",    null: false
    t.text     "body",              default: "",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "published",         default: false, null: false
    t.string   "slug",              default: "",    null: false
    t.text     "short_description"
    t.text     "image"
  end

  create_table "ad_posts_newsletters", id: false, force: true do |t|
    t.integer "newsletter_id"
    t.integer "ad_post_id"
  end

  add_index "ad_posts_newsletters", ["newsletter_id"], name: "index_ad_posts_newsletters_on_newsletter_id", using: :btree

  create_table "auctions", force: true do |t|
    t.string   "title",                 default: "",    null: false
    t.text     "body",                  default: "",    null: false
    t.date     "date",                                  null: false
    t.string   "address",               default: "",    null: false
    t.float    "longitude"
    t.float    "latitude"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "published",             default: false, null: false
    t.string   "address_for_geocoding", default: "",    null: false
    t.string   "pdf"
    t.string   "catalog"
    t.string   "slug",                  default: "",    null: false
    t.text     "short_description"
    t.text     "image"
    t.string   "start_time"
    t.text     "google_maps_url"
  end

  create_table "auctions_newsletters", id: false, force: true do |t|
    t.integer "newsletter_id"
    t.integer "auction_id"
  end

  add_index "auctions_newsletters", ["newsletter_id"], name: "index_auctions_newsletters_on_newsletter_id", using: :btree

  create_table "images", force: true do |t|
    t.integer  "parent_id",                null: false
    t.string   "parent_type",              null: false
    t.string   "file"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "position",    default: 0,  null: false
    t.string   "caption",     default: "", null: false
  end

  add_index "images", ["parent_id", "parent_type", "position"], name: "index_images_on_parent_id_and_parent_type_and_position", using: :btree
  add_index "images", ["parent_id", "parent_type"], name: "index_images_on_parent_id_and_parent_type", using: :btree

  create_table "newsletters", force: true do |t|
    t.datetime "sent_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "body",       default: ""
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
