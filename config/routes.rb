Mepalex::Application.routes.draw do
  devise_for :users

  # Administration routes
  get 'admin' => redirect('/admin/auctions')
  namespace :admin do
    resources :auctions do
      resources :status_updates, :only => [:new, :create]
      resources :images
    end
    resources :ad_posts do
      resources :status_updates, :only => [:new, :create]
      resources :images
    end
    resources :newsletters do
      post 'mail', :on => :member
    end
    resources :images do
      post 'sort', :on => :collection
    end
  end

  # Public routes
  get   'contact'          => 'contact_messages#new',    :as => 'new_contact_message'
  post  'contact'          => 'contact_messages#create', :as => 'contact_messages'
  get   'encans/:slug'     => 'auctions#show',           :as => 'auction'
  get   'encans'           => 'auctions#index',          :as => 'auctions'
  get   'babillard/:slug'  => 'ad_posts#show',           :as => 'ad_post'
  get   'babillard'        => 'ad_posts#index',          :as => 'ad_posts'
  get   'a-propos'         => 'about#index',             :as => 'about'
  get   'mail/:newsletter_id' => 'homepage#mail',        :as => 'mail'

  root :to => 'homepage#show'
end
