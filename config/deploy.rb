require "bundler/capistrano"
require "dotenv/deployment/capistrano"
require 'capistrano/sidekiq'

set :application, "paul-hus.com"
set :repository,  "git@bitbucket.org:hydrozen/mepalex.git"
set :branch, "master"
set :scm, :git
set :deploy_to, "/var/www/paul-hus.com"
set :deploy_via, :remote_cache
set :user, 'patrick'
set :use_sudo, false
set :keep_releases, 4
set :bundle_flags, "--deployment --quiet"
set :default_environment, 'PATH' => "/home/patrick/.rbenv/shims:/home/patrick/.rbenv/bin:$PATH"
server "75.101.144.120", :app, :web, :db, :primary => true

# This is for rails 4 new handling of binstubs
set :bundle_binstubs, nil
set :linked_dirs, %w(log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system)

namespace :deploy do
  task :start do
    run "#{release_path}/script/thin start"
  end
  task :stop do
    run "#{release_path}/script/thin stop"
  end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{release_path}/script/thin restart"
  end
end

desc "Clears the cache"
task :clear_cache do
  puts "clearing the cache..."
  run "cd #{deploy_to}/current && /usr/bin/env bundle exec rake app:cache:clear RAILS_ENV=production"
end

desc "Create symlink to various resources"
task :create_symlinks do
  run "ln -s #{deploy_to}/shared/config/database.yml #{release_path}/config/database.yml"
  run "ln -s #{deploy_to}/shared/config/skylight.yml #{release_path}/config/skylight.yml"
  run "ln -s #{deploy_to}/shared/cache #{release_path}/public/cache"
  run "ln -s #{deploy_to}/shared/uploads #{release_path}/public/uploads"
  run "ln -s #{deploy_to}/shared/config/initializers/errbit.rb #{release_path}/config/initializers/errbit.rb"
end

after "deploy:finalize_update", "create_symlinks"
after "deploy:restart", "deploy:cleanup", "clear_cache"
