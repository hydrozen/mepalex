module CarrierWave
  module MiniMagick
    def quality(percentage)
      manipulate! do |img|
        img.quality(percentage.to_s)
        img = yield(img) if block_given?
        img
      end
    end

    def optimize
      image_optim = ImageOptim.new(:pngout => false, :svgo => false, pngquant: false)
      image_optim.optimize_image!(current_path)
    end
  end
end
