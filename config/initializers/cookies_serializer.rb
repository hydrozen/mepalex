# This would transparently migrate your existing Marshal-serialized cookies into the new JSON-based format.
# See http://guides.rubyonrails.org/upgrading_ruby_on_rails.html
# TODO: Set to :json eventually.
Rails.application.config.action_dispatch.cookies_serializer = :json
