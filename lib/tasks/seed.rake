namespace :seed do
  desc "Create dummy auctions"
  task :auctions => :environment do
    10.times do
      auction = Auction.new
      auction.title = Faker::Lorem.words(Random.rand(4..10)).join(" ").capitalize
      auction.body = Faker::Lorem.paragraphs(Random.rand(1..2)).join("\n\n")
      auction.date = Random.rand(1..40).days.from_now
      auction.address = "Saint-Hyacinthe, Québec"
      auction.address_for_geocoding = "635 Papineau Saint-Hyacinthe, Québec J2S 7J5"
      auction.published = true
      auction.slug = auction.title
      auction.save
    end
  end
end
