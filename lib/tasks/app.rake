namespace :app do
  namespace :cache do
    desc "Clears the cached pages"
    task :clear => :environment do
      FileUtils.rm_rf "#{Rails.root}/public/cache/."
    end
  end
end
