source 'https://rubygems.org'

# This needs to be loaded before anything else that uses it.
gem 'dotenv-rails'

gem 'rails', '~> 4.1.0'
gem 'pg'
gem 'devise', '~> 3.0.0'
gem 'geocoder', '~> 1.2.2'
gem 'twitter', '~> 5.0'
gem 'googl'
gem 'koala', '~> 1.9'
gem 'mailchimp-api', '~> 2.0.5', require: 'mailchimp'
gem 'carrierwave', '~> 0.9'
gem 'mini_magick'
gem 'kaminari', '~> 0.16.1'
gem 'rest_in_place'
gem 'redcarpet'
gem 'aws-ses', '~> 0.6', require: 'aws/ses'
gem 'capistrano', '~> 2.0', require: false
gem 'whenever', '~> 0.9.2', require: false
gem 'thin', require: false
gem 'airbrake', '~> 4.0'
gem 'skylight'
gem 'meta-tags'
gem 'image_optim'
gem 'sidekiq'

# Assets
gem 'jquery-rails', '~> 3.0'
gem 'jquery-ui-rails', '~> 5.0'
gem 'sass-rails', '4.0.3'
gem 'compass-rails', '2.0.0'
gem 'bootstrap-sass', '~> 3.3.0'
gem 'coffee-rails'
gem 'uglifier'

# Rails features that were extracted to gems
gem 'actionpack-action_caching', '~> 1.1.1'
gem 'actionpack-page_caching', '~>1.0.0'
gem 'actionview-encoded_mail_to', '~>1.0.4'
gem 'rails-observers', '~>0.1.1'
# gem 'activerecord-session_store', '~>0.0.1'

group :development do
  gem 'letter_opener'
  gem 'better_errors'
  gem 'rubocop', require: false
  gem 'capistrano-sidekiq'
end

group :development, :test do
  gem 'rspec-rails', '~> 3.0.2'
  gem 'faker'
  gem 'byebug'
end

group :test do
  gem 'factory_girl_rails'
  gem 'capybara', '~> 2.2'
  gem 'webmock'
  gem 'vcr'
  gem 'database_cleaner'
end

group :production do
  gem 'dotenv-deployment'
end
