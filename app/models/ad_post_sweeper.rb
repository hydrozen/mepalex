class AdPostSweeper < ActionController::Caching::Sweeper
  observe AdPost

  def after_create(ad_post)
    expire ad_post
  end

  def after_update(ad_post)
    expire ad_post
  end

  def after_destroy(ad_post)
    expire ad_post
  end

  private

  def expire(ad_post)
    ActionController::Base.new.expire_page '/'
    ActionController::Base.new.expire_page '/babillard'
    ActionController::Base.new.expire_page "/babillard/#{ad_post.slug_with_id}"
  end
end
