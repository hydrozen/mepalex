class Newsletter < ActiveRecord::Base
  has_and_belongs_to_many :auctions
  has_and_belongs_to_many :ad_posts

  def mail
    I18n.with_locale(:fr) do
      create_campaign
    end
  end

  protected

  def create_campaign
    api = Mailchimp::API.new(Mepalex::Application.config.mailchimp[:api_key])
    campaign = api.campaigns.create "regular", campaign_options, campaign_content
    api.campaigns.send(campaign["id"])
  end

  def campaign_options
    {
      :list_id => '159924ed0d',
      :subject => "Lettre d'information du #{I18n.l(Date.current, :format => :long)}",
      :from_email => 'noreply@paul-hus.com',
      :from_name => 'Daniel Paul-Hus',
      :inline_css => true
    }
  end

  def campaign_content
    mailer = NewsletterMailer.newsletter(self)
    content = {
      :html => mailer.html_part.body.to_s,
      :text => mailer.text_part.body.to_s
    }
  end
end
