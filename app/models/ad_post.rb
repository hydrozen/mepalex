class AdPost < ActiveRecord::Base
  # Uploader used to attach a showcase image for a post.
  mount_uploader :image, ImageUploader

  # The image gallery associated to this post.
  has_many :images, -> { order('position ASC') }, :as => :parent, :dependent => :destroy

  # Returns only the posts that are published.
  #
  # @!method published
  # @!scope class
  # @return [ActiveRecord::Relation]
  scope :published, -> { where(:published => true) }

  # Returns the posts in reverse chronological order of their
  # creation date.
  #
  # @!method oldest_first
  # @!scope class
  # @return [ActiveRecord::Relation]
  scope :oldest_first, -> { order('created_at DESC') }

  # Returns a slug that looks like '5-the-auction-title'
  #
  # @return [String] the slug for this auction
  def slug_with_id
    "#{id}-#{slug}"
  end

  def skip_processing
    false
  end

  # Sets the slug of this post. This setter makes sure
  # that the slug is URI friendly. The slug doesn’t need
  # to be unique since we always use it with a prefixed id.
  def slug=(slug)
    write_attribute :slug, slug.parameterize
  end

  # Finds a post using a slug with id.
  #
  # @param slug [String] The slug of the post prefixed by its ID.
  # @return [AdPost] If a post was found.
  # @return [nil] If a post was not found.
  def self.find_by_slug_with_id(slug)
    where(:id => slug.split("-").first.to_i).limit(1).first
  end

  # Returns all the posts for display on the listing page.
  #
  # @return [AdPost]
  def self.all_for_listing
    published.oldest_first
  end
end
