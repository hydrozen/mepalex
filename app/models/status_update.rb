class StatusUpdate
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  attr_accessor :status, :link

  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def persisted?
    false
  end

  def post
    post_to_twitter
    post_to_facebook
  end

  def post_to_twitter
    Twitter.update("#{status} #{link}")
  end

  def post_to_facebook
    token = facebook_page_token
    page = Koala::Facebook::API.new(token)
    page.put_wall_post(status, :link => link)
  end

  private

  def facebook_page_token
    user = Koala::Facebook::API.new(Mepalex::Application.config.facebook[:token])
    user.get_connections('me', 'accounts').find { |p| p['id'] == ENV['FACEBOOK_PAGE_ID'] }['access_token']
  end
end
