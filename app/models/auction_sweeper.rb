class AuctionSweeper < ActionController::Caching::Sweeper
  observe Auction

  def after_create(auction)
    expire auction
  end

  def after_update(auction)
    expire auction
  end

  def after_destroy(auction)
    expire auction
  end

  private

  def expire(auction)
    ActionController::Base.new.expire_page '/'
    ActionController::Base.new.expire_page '/encans'
    ActionController::Base.new.expire_page "/encans/#{auction.slug_with_id}"
  end
end
