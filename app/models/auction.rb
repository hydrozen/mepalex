class Auction < ActiveRecord::Base
  # Validation
  validates_presence_of :title, :date, :address, :slug

  attr_accessor :skip_processing

  # Defines which attribute is used to try to find the longitude
  # and latitude of this auction.
  geocoded_by :address_for_geocoding

  # The primary image of this auction. Used in the listings and such.
  mount_uploader :image, ImageUploader

  # The PDF version of the detailed text of this auction.
  mount_uploader :pdf, PdfUploader

  # The PDF version of the catalog of this auction.
  mount_uploader :catalog, PdfUploader

  # The images associated with this auction. The user can define
  # the order of the images using the position attribute.
  has_many :images, -> { order('position ASC') }, :as => :parent, :dependent => :destroy

  # Callbacks
  after_validation :geocode

  # Scopes
  scope :upcoming, -> { where("date >= ?", Date.current).ordered_by_date }
  scope :published, -> { where(:published => true) }
  scope :ordered_by_date, -> { order("date ASC") }

  class << self
    # Finds an auction using a slug with id.
    #
    # @param slug [String] The slug of the auction prefixed by its ID.
    # @return [Auction] If a post was found.
    # @return [nil] If a post was not found.
    def find_by_slug_with_id(slug)
      where(:id => slug.split("-").first.to_i).limit(1).first
    end
  end

  def skip_processing
    false
  end

  # Returns the url to the google map for this auction.
  #
  # @return [String] the url if the auction has coordinates.
  # @return [nil] if the auction doesn’t have coordinates.
  def google_map
    "http://maps.google.com/maps?q=#{latitude}+#{longitude}" if geocoded?
  end

  # @return [Boolean] is the auction geocoded?
  def geocoded?
    return true if google_maps_url
    !!(latitude && longitude)
  end

  # Returns a slug that looks like '5-the-auction-title'
  # @return [String] the slug for this auction
  def slug_with_id
    "#{id}-#{slug}"
  end

  def google_maps_url=(url)
    matches = url.match(/src="(\S+)"/)
    url = matches[1] if matches
    write_attribute(:google_maps_url, url)
  end

  # Sets the slug of this auction. It makes sure that the slug
  # is URI friendly.
  def slug=(slug)
    write_attribute :slug, slug.parameterize
  end

  # Returns true if the date of the auction is passed.
  #
  # @return [Boolean] is the auction passed or not.
  def outdated?
    Date.current > date
  end

  # Custom setter because my mom keeps putting spaces before/after
  # parenthesises when she should not.
  def title=(value)
    value.gsub!(/\( /, '(')
    value.gsub!(/ \)/, ')')
    write_attribute(:title, value)
  end
end
