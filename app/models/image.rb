class Image < ActiveRecord::Base
  # An image is always associated with a parent object.
  belongs_to :parent, polymorphic: true, touch: true
  attr_accessor :skip_processing

  # The uploader for the actual image.
  mount_uploader :file, ImageUploader

  # Sets the position field that is used for the ordering
  # of the images.
  before_create :set_position
  after_create :process

  def recreate_delayed_versions!
    self.skip_processing = false
    file.recreate_versions!
  end

  def skip_processing
    return @skip_processing = true if @skip_processing.nil?
    @skip_processing
  end

  private

  # Sets the position index of the image to be the last
  # one.
  def set_position
    max_position = parent.images.maximum("position")
    new_position = max_position.nil? ? 1 : max_position + 1
    self.position = new_position
  end

  def process
    return unless skip_processing
    ImageProcessingWorker.perform_async(id)
  end
end
