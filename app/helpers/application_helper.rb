module ApplicationHelper
  # Renders the given markdown has safe html.
  def markdown(content)
    content = "" if content.nil?
    markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML)
    markdown.render(content).html_safe
  end

  # Sets the meta description.
  def description(desc)
    content_for(:description) { desc }
  end

  # Sets the page title.
  def page_title(title = nil)
    if title.nil?
      if content_for? :page_title
        content_tag :title, "#{content_for(:page_title)} | Encans Daniel Paul-Hus | Encanteur Bilingue spécialisé dans l'industrie agricole au Québec"
      else
        content_tag :title, "Encans Daniel Paul-Hus | Encanteur Bilingue spécialisé dans l'industrie agricole au Québec"
      end
    else
      content_for(:page_title) { title }
    end
  end

  # Sets the page description.
  def page_description(desc = nil)
    content_for(:page_description) { desc }
  end

  # Renders a link to a phone number if the user is on mobile. If not,
  # just print the number.
  def link_to_phone_number(number)
    if ENV["X_MOBILE_DEVICE"]
      link_to number, "tel:+1#{number.gsub('-', '').gsub(' ', '')}"
    else
      number
    end
  end
end
