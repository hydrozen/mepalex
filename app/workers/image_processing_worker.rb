class ImageProcessingWorker
  include Sidekiq::Worker

  def perform(image_id)
    image = Image.find(image_id)
    image.recreate_delayed_versions!
  end
end
