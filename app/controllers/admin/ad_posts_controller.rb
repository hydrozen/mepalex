class Admin::AdPostsController < Admin::ApplicationController
  cache_sweeper :ad_post_sweeper

  def index
    @ads = AdPost.unscoped.order("id DESC").page(params[:page]).per(25)
  end

  def new
    @ad = AdPost.unscoped.new
  end

  def create
    @ad = AdPost.unscoped.create(ad_post_params)
    @ad.save
    redirect_to [:admin, @ad]
  end

  def edit
    @ad = AdPost.unscoped.find(params[:id])
  end

  def update
    @ad = AdPost.unscoped.find(params[:id])
    @ad.update_attributes(ad_post_params)
    redirect_to [:admin, @ad]
  end

  def show
    @ad = AdPost.unscoped.find(params[:id])
  end

  def destroy
    @ad = AdPost.unscoped.find(params[:id])
    @ad.destroy
    redirect_to [:admin, :ad_posts]
  end

  private

  def ad_post_params
    safe_attributes = [
      :title, :slug, :short_description, :body, :image, :published
    ]
    params.require(:ad_post).permit(safe_attributes)
  end
end
