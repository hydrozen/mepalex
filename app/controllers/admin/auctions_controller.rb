class Admin::AuctionsController < Admin::ApplicationController
  cache_sweeper :auction_sweeper
  before_filter :find_auction, :only => [:edit, :update, :destroy, :show]

  def index
    @auctions = Auction
                .order("date ASC")
                .where("date >= ?", Date.today)
                .page(params[:page])
                .per(25)
  end

  def new
    @auction = Auction.new
    @auction.published = false
  end

  def create
    @auction = Auction.create(auction_params)
    redirect_to [:admin, @auction]
  end

  def edit; end

  def update
    @auction.update_attributes(auction_params)
    redirect_to [:admin, @auction]
  end

  def destroy
    @auction.destroy
    redirect_to [:admin, :auctions]
  end

  def show; end

  private

  def find_auction
    @auction = Auction.find(params[:id])
  end

  def auction_params
    safe_attributes = [
      :title, :slug, :short_description, :body, :address,
      :image, :pdf, :catalog, :date, :start_time, :published,
      :google_maps_url, :address_for_geocoding
    ]
    params.require(:auction).permit(safe_attributes)
  end
end
