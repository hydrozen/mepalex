class Admin::NewslettersController < Admin::ApplicationController
  before_filter :find_newsletter, :only => [:edit, :update, :destroy, :show, :mail]

  def index
    @newsletters = Newsletter.order("created_at DESC")
  end

  def new
    @newsletter = Newsletter.new
  end

  def create
    @newsletter = Newsletter.new(newsletter_params)
    if @newsletter.save
      redirect_to admin_newsletters_path
    else
      render :new
    end
  end

  def edit
  end

  def update
    @newsletter.update_attributes(newsletter_params)
    if @newsletter.save
      redirect_to admin_newsletters_path
    else
      render :edit
    end
  end

  def destroy
    @newsletter.destroy
    redirect_to admin_newsletters_path
  end

  def show
  end

  def mail
    @newsletter.mail
    redirect_to admin_newsletters_path
  end

  protected

  def find_newsletter
    @newsletter = Newsletter.find(params[:id])
  end

  def newsletter_params
    safe_attributes = [:body, :caption, :auction_ids => [], :ad_post_ids => []]
    params.require(:newsletter).permit(safe_attributes)
  end
end
