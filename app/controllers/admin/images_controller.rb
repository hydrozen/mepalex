class Admin::ImagesController < Admin::ApplicationController
  def create
    process_images_param!

    respond_to do |format|
      format.html { redirect_to :back }
      format.json { render :json => @image }
    end
  end

  def update
    @image = Image.find(params[:id])
    @image.update_attributes(image_params)
    respond_to do |format|
      format.html { redirect_to :back }
      format.json { render :json => @image }
    end
  end

  def destroy
    @image = Image.find(params[:id])
    @image.destroy
    redirect_to :back
  end

  def sort
    params[:image].each_with_index do |id, index|
      Image.where(:id => id).update_all(:position => index + 1)
    end
    render :nothing => true
  end

  private

  def image_params
    safe_attributes = [:file, :caption]
    params.require(:image).permit(safe_attributes)
  end

  def process_images_param!
    return unless params[:images]
    params[:images].each do |image|
      @image = Image.new(:skip_processing => true)
      @image.file = image
      @image.parent_type = params[:parent_type]
      @image.parent_id   = params[:parent_id]
      @image.save
    end
  end
end
