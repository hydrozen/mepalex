class Admin::ApplicationController < ActionController::Base
  protect_from_forgery
  layout 'admin/application'
  before_filter :set_locale
  before_filter :authenticate_user!

  protected

  def set_locale
    I18n.locale = :en
  end
end
