class Admin::StatusUpdatesController < Admin::ApplicationController
  before_filter :find_object, :only => [:new]

  def new
    @status = StatusUpdate.new
    url = Googl.shorten(object_url).short_url
    @status.status = "#{@object.title}"
    @status.link = url
  end

  def create
    @status = StatusUpdate.new params[:status_update]
    @status.post
    redirect_to :back, :notice => "Your status update has been posted."
  end

  protected

  def find_object
    if params.keys.include? 'auction_id'
      @object = Auction.find(params[:auction_id])
    elsif params.keys.include? 'ad_post_id'
      @object = AdPost.find(params[:ad_post_id])
    end
  end

  def object_url
    case @object.class.to_s
    when 'Auction'
      auction_url(@object)
    when 'AdPost'
      ad_post_url(@object)
    end
  end
end
