class AdPostsController < ApplicationController
  caches_page :index, :show

  def index
    @posts = AdPost.all_for_listing
  end

  def show
    @post = AdPost.find(params[:slug])
  end
end
