class AuctionsController < ApplicationController
  caches_page :index, :show

  def index
    @auctions = Auction.published.upcoming
  end

  def show
    @auction = Auction.find_by_slug_with_id(params[:slug])
    raise ActiveRecord::RecordNotFound unless @auction
    raise ActiveRecord::RecordNotFound if @auction.outdated?
  end
end
