class HomepageController < ApplicationController
  caches_page :show

  def show
    @auctions = Auction
                .where(:published => true)
                .where(["date >= ?", Date.today])
                .order("date ASC")
                .includes(:images)
                .limit(2)
    @ads = AdPost
           .where(:published => true)
           .order("created_at DESC")
           .includes(:images)
           .limit(2)
  end

  def mail
    @newsletter = Newsletter.find(params[:newsletter_id])
    render :template => 'newsletter_mailer/newsletter', :layout => false
  end
end
