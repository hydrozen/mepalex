class ContactMessagesController < ApplicationController
  def new
    @message = ContactMessage.new
  end

  def create
    @message = ContactMessage.new(params[:contact_message])
    if @message.valid?
      ContactMessageMailer.message_from_website(@message).deliver
      redirect_to new_contact_message_path, :notice => "Votre message a été envoyé."
    else
      render :new
    end
  end
end
