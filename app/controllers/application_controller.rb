class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  layout :layout_by_resource
  before_filter :set_locale

  protected

  def set_locale
    I18n.locale = devise_controller? ? :en : :fr
  end

  def layout_by_resource
    if devise_controller?
      "admin/application"
    else
      "application"
    end
  end
end
