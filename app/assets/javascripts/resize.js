$(window).load(function() {
  $('.item-box').each(function(index, value) {
    var $value = $(value);
    var targetHeight = $value.parent().parent().height();
    $value.parent().height(targetHeight);
    $value.height(targetHeight);
  });

  // Track downloads
  $('ul.downloads a').click(function() {
    console.log("downloading pdf...");
    _gaq.push(['_trackEvent','Download','PDF', this.href]);
  });
});
