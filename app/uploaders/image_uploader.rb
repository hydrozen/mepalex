class ImageUploader < CarrierWave::Uploader::Base
  # Include RMagick or MiniMagick support:
  include CarrierWave::MiniMagick

  # Include the Sprockets helpers for Rails 3.1+ asset pipeline compatibility:
  # include Sprockets::Helpers::RailsHelper
  # include Sprockets::Helpers::IsolatedHelper

  # Include the sprockets-rails helper for Rails 4+ asset pipeline compatibility:
  include Sprockets::Rails::Helper

  # Choose what kind of storage to use for this uploader:
  storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  process :quality => 60, :if => :process_that_shit?
  process :optimize, :if => :process_that_shit?

  version :thumb, :if => :process_that_shit? do
    process :resize_to_fill => [160, 120]
  end

  version :small_square do
    process :resize_to_fill => [50, 50]
  end

  version :scaled, :if => :process_that_shit? do
    process :resize_to_fit => [460, 345]
  end

  version :big, :if => :process_that_shit? do
    process :resize_to_fit => [920, 690]
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  def can_be_compressed?(image)
    i = MiniMagick::Image.open(image.path)
    quality = i['%Q'].to_i
    quality > 60 && image.content_type == 'image/jpeg'
  end

  def process_that_shit?(_img = nil)
    !model.skip_processing
  end

  def skip_processing?(_img = nil)
    model.skip_processing
  end
end
