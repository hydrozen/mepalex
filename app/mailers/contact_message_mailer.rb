class ContactMessageMailer < ActionMailer::Base
  default from: "noreply@paul-hus.com"

  # Sends a message from the website to Daniel
  def message_from_website(msg)
    @msg = msg
    subject = "Message provenant du site web de la part de #{msg.name}"
    mail :to => "daniel@paul-hus.com", :reply_to => msg.email,
         :subject => subject
  end
end
