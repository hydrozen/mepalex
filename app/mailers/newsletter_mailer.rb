class NewsletterMailer < ActionMailer::Base
  default from: "noreply@paul-hus.com"
  helper :application

  def newsletter(newsletter)
    @newsletter = newsletter
    mail :to => "foo@paul-hus.com"
  end
end
